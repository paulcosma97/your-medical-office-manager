package ro.uvt.info.ymom.utils;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import javax.annotation.PostConstruct;
import java.io.FileOutputStream;
import java.io.OutputStream;

@Slf4j
@Component
@RequiredArgsConstructor
public class PDFWriter {

    private final TemplateEngine templateEngine;

    @SneakyThrows
    public void createPatientReport(Context context) {
        String html = templateEngine.process("patient-report", context);
        OutputStream outputStream = new FileOutputStream("report.pdf");
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(html);
        renderer.layout();
        renderer.createPDF(outputStream);
        outputStream.close();
    }


}
