package ro.uvt.info.ymom.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ro.uvt.info.ymom.models.AppointmentOutDto;
import ro.uvt.info.ymom.models.Role;
import ro.uvt.info.ymom.models.UserInDto;
import ro.uvt.info.ymom.models.UserOutDto;
import ro.uvt.info.ymom.services.UserService;
import ro.uvt.info.ymom.utils.CrudController;

import java.util.List;

@RestController
@RequestMapping("/doctors")
@RequiredArgsConstructor
public class DoctorController implements CrudController<UserInDto, UserOutDto> {
    private final UserService userService;

    @GetMapping
    public List<UserOutDto> getAll() {
        return userService.getUsers(Role.DOCTOR);
    }


    @GetMapping("/{id}")
    public UserOutDto getOne(@PathVariable long id) {
        return userService.getUser(id);
    }

    @PostMapping
    public UserOutDto create(@RequestBody UserInDto userInDto) {
        return userService.createUser(userInDto, Role.DOCTOR);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        userService.deleteUser(id);
    }

    @PutMapping("/{id}")
    public UserOutDto update(@PathVariable long id, @RequestBody UserInDto userInDto) {
        return userService.updateUser(id, userInDto);
    }

    @GetMapping("/{id}/history")
    public List<AppointmentOutDto> getAppointmentHistory(@PathVariable long id) {
        return userService.getDoctorAppointmentHistory(id);
    }

    @GetMapping("/{id}/upcoming-appointments")
    public List<AppointmentOutDto> getUpcomingAppointments(@PathVariable long id) {
        return userService.getAllUpcomingDoctorAppointments(id);
    }

}
