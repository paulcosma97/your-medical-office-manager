package ro.uvt.info.ymom.utils;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import ro.uvt.info.ymom.entities.User;
import ro.uvt.info.ymom.exceptions.InvalidRequestException;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@Slf4j
@UtilityClass
public class Utilitus {
    public void validateCnp(String cnp) {
        val birth = getBirthDateFromCnp(cnp);
        if(birth.isAfter(LocalDate.now())) {
            log.error("Given patient CNP is invalid. Birth date is in the future");
            throw new InvalidRequestException("Given patient CNP is invalid. Birth date is in the future");
        }

        List<String> countyCodes = Arrays.asList("01" /*Alba*/,"02" /*Arad*/,"03" /*Argeș*/,"04" /*Bacău*/,"05" /*Bihor*/,"06" /*Bistrița-Năsăud*/,"07" /*Botoșani*/,"08" /*Brașov*/,"09" /*Brăila*/,"10" /*Buzău*/,"11" /*Caraș-Severin*/,"12" /*Cluj*/,"13" /*Constanța*/,"14" /*Covasna*/,"15" /*Dâmbovița*/,"16" /*Dolj*/,"17" /*Galați*/,"18" /*Gorj*/,"19" /*Harghita*/,"20" /*Hunedoara*/,"21" /*Ialomița*/,"22" /*Iași*/,"23" /*Ilfov*/,"24" /*Maramureș*/,"25" /*Mehedinți*/,"26" /*Mureș*/,"27" /*Neamț*/,"28" /*Olt*/,"29" /*Prahova*/,"30" /*Satu Mare*/,"31" /*Sălaj*/,"32" /*Sibiu*/,"33" /*Suceava*/,"34" /*Teleorman*/,"35" /*Timiș*/,"36" /*Tulcea*/,"37" /*Vaslui*/,"38" /*Vâlcea*/,"39" /*Vrancea*/,"40" /*București*/,"41" /*București - Sector 1*/,"42" /*București - Sector 2*/,"43" /*București - Sector 3*/,"44" /*București - Sector 4*/,"45" /*București - Sector 5*/,"46" /*București - Sector 6*/,"51" /*Călărași*/,"52"  /*Giurgiu*/);

        if( countyCodes.parallelStream().noneMatch(code -> code.equals(cnp.substring(7, 9))) ) {
            log.error("Given cnp is invalid. County code unrecognized");
            throw new InvalidRequestException("Given CNP is invalid. County code unrecognized");
        }

        val controlDigit = calculateControlDigit(cnp.toCharArray());
        if(!cnp.substring(12, 13).equals(controlDigit + "")) {
            throw new InvalidRequestException("Given CNP is invalid. Control digit does not match");
        }

        if(cnp.substring(0, 1).equals("0")) {
            throw new InvalidRequestException("Given CNP is invalid. First digit can't be 0");
        }
    }

    public LocalDate getBirthDateFromCnp(String cnp) {
        try {
            return LocalDate.of(Integer.parseInt(cnp.substring(1, 3)), Integer.parseInt(cnp.substring(3, 5)), Integer.parseInt(cnp.substring(5, 7)))
                    .plusYears(cnp.substring(0, 1).equals("5") || cnp.substring(0, 1).equals("6") ? 2000 : 1900);
        } catch (DateTimeException e) {
            log.error("Could not parse CNP {}. Invalid dates", cnp);
            throw new InvalidRequestException("Given CNP has an invalid format");
        }
    }

    private int calculateControlDigit(char[] chars) {
        int[] ints = new int[] { 2, 7, 9, 1, 4, 6, 3, 5, 8, 2, 7, 9 };

        int result = 0;
        for(int i = 0; i < ints.length; i++) {
            int digit1 = chars[i] - '0';
            int digit2 = ints[i];

            result += digit1 * digit2;
        }

        while(result >= 11) {
            result -= 11;
        }

        if(result == 10) {
            result = 1;
        }

        return result;
    }
}
