package ro.uvt.info.ymom.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppointmentInDto {
    private long patientId;
    private List<Long> assistanceIds;
    private UnregisteredPatientInDto unregisteredPatient;
    private boolean registeredPatient;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime time;
}
