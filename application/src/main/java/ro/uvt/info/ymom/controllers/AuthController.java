package ro.uvt.info.ymom.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.uvt.info.ymom.models.AuthCredentials;
import ro.uvt.info.ymom.models.UserOutDto;
import ro.uvt.info.ymom.services.AuthService;

@RestController
@RequestMapping("/login")
@RequiredArgsConstructor
public class AuthController {
    private final AuthService authService;

    @PostMapping
    public UserOutDto login(@RequestBody AuthCredentials authCredentials) {
        return authService.canLogin(authCredentials);
    }
}
