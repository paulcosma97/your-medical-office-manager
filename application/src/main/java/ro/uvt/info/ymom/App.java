package ro.uvt.info.ymom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.convert.Jsr310Converters;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

@SpringBootApplication
@EntityScan(basePackageClasses = { App.class, Jsr310JpaConverters.class })
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
