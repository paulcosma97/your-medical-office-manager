package ro.uvt.info.ymom.services;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import ro.uvt.info.ymom.entities.Appointment;
import ro.uvt.info.ymom.entities.Assistance;
import ro.uvt.info.ymom.entities.User;
import ro.uvt.info.ymom.exceptions.InvalidRequestException;
import ro.uvt.info.ymom.exceptions.UserNotFoundException;
import ro.uvt.info.ymom.mappers.AppointmentMapper;
import ro.uvt.info.ymom.mappers.UserMapper;
import ro.uvt.info.ymom.models.*;
import ro.uvt.info.ymom.repositories.AppointmentRepository;
import ro.uvt.info.ymom.repositories.UserRepository;
import ro.uvt.info.ymom.utils.PDFWriter;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final AppointmentMapper appointmentMapper;
    private final AppointmentRepository appointmentRepository;
    private final PasswordEncoder passwordEncoder;
    private final PDFWriter pdfWriter;

    public List<UserOutDto> getUsers(Role role) {
        return userRepository.findAllByRolesContaining(role).parallelStream().map(userMapper::toOutbound).collect(Collectors.toList());
    }

    public UserOutDto getUser(long id) {
        return userMapper.toOutbound(userRepository.findById(id).orElseThrow(UserNotFoundException::new));
    }

    public UserOutDto createUser(UserInDto userIn, Role role) {
        val user = userMapper.fromOutbound(userIn);
        user.setRoles(Arrays.asList(role));
        return userMapper.toOutbound(userRepository.save(user));
    }

    public void deleteUser(long id) {
        val user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        userRepository.delete(user);
    }

    public UserOutDto updateUser(long id, UserInDto userIn) {
        val user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        val fromIn = userMapper.fromOutbound(userIn);

        user
            .setBirthday(fromIn.getBirthday())
            .setCnp(fromIn.getCnp())
            .setFirstName(fromIn.getFirstName())
            .setLastName(fromIn.getLastName())
            .setPassword(fromIn.getPassword());

        return userMapper.toOutbound(userRepository.save(user));
    }

    public List<AppointmentOutDto> getPatientAppointmentHistory(long id) {
        val user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        if(!user.getRoles().contains(Role.PATIENT)) {
            throw new InvalidRequestException("User is not a patient");
        }

        return user.getPatientAppointments().parallelStream()
                .filter(app -> app.getTime().isBefore(LocalDateTime.now()))
                .map(appointmentMapper::toOutbound)
                .collect(Collectors.toList());
    }

    public List<AppointmentOutDto> getDoctorAppointmentHistory(long id) {
        val user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        if(!user.getRoles().contains(Role.DOCTOR)) {
            throw new InvalidRequestException("User is not a doctor");
        }

        return user.getDoctorAppointments().parallelStream()
                .filter(app -> app.getTime().isBefore(LocalDateTime.now()))
                .map(appointmentMapper::toOutbound)
                .collect(Collectors.toList());
    }

    public PatientReport getPatientReport(long id) {
        val user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        if(!user.getRoles().contains(Role.PATIENT)) {
            throw new InvalidRequestException("User is not a patient");
        }

        return calculateReports(Collections.singletonList(user)).get(0);
    }

    public List<PatientReport> getAllPatientReports() {
        return calculateReports(userRepository.findAllByRolesContaining(Role.PATIENT));
    }

    private List<PatientReport> calculateReports(List<User> users) {
        return users.parallelStream()
                .map(user -> {
                    val report = new PatientReport();
                    report.setUser(userMapper.toOutbound(user));
                    report.setTotalAmount(user.getPatientAppointments()
                            .parallelStream()
                            .mapToDouble(app -> app.getAssistances()
                                    .parallelStream()
                                    .mapToDouble(Assistance::getPrice)
                                    .sum())
                            .sum());
                    report.setLastYear(user.getPatientAppointments()
                            .parallelStream()
                            .filter(appointment -> appointment.getTime().isAfter(appointment.getTime().minusYears(1)))
                            .mapToDouble(app -> app.getAssistances()
                                    .parallelStream()
                                    .mapToDouble(Assistance::getPrice)
                                    .sum())
                            .sum());
                    report.setLastMonth(user.getPatientAppointments()
                            .parallelStream()
                            .filter(appointment -> appointment.getTime().isAfter(appointment.getTime().minusMonths(1)))
                            .mapToDouble(app -> app.getAssistances()
                                    .parallelStream()
                                    .mapToDouble(Assistance::getPrice)
                                    .sum())
                            .sum());
                    report.setLastDay(user.getPatientAppointments()
                            .parallelStream()
                            .filter(appointment -> appointment.getTime().isAfter(appointment.getTime().minusDays(1)))
                            .mapToDouble(app -> app.getAssistances()
                                    .parallelStream()
                                    .mapToDouble(Assistance::getPrice)
                                    .sum())
                            .sum());
                    return report;
                })
                .collect(Collectors.toList());
    }

    public List<AppointmentOutDto> getAllUpcomingDoctorAppointments(long id) {
        val user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        if(!user.getRoles().contains(Role.DOCTOR)) {
            throw new InvalidRequestException("User is not a doctor");
        }

        return appointmentRepository.findAllUpcomingByDoctor(user).stream().map(appointmentMapper::toOutbound).collect(Collectors.toList());
    }


    public UserOutDto getUserByCnp(String cnp) {
        return userMapper.toOutbound(userRepository.findByCnp(cnp).orElseThrow(UserNotFoundException::new));
    }

    @Transactional
    public UserOutDto saveUnregistered(long appId, UserInDto userInDto) {
        val appointment = appointmentRepository.findById(appId).orElseThrow(() -> new EntityNotFoundException("Appointment not found"));

        val existing = appointment.getUnregisteredPatient();
        if(existing == null) {
            throw new EntityNotFoundException("Appointment does not have an unregistered patient");
        }

        val patient = new User()
//                .setId(userRepository.findLastId() + 2)
                .setBirthday(existing.getBirthday())
                .setCnp(existing.getCnp())
                .setFirstName(existing.getFirstName())
                .setLastName(existing.getLastName())
                .setPassword(passwordEncoder.encode(userInDto.getPassword()))
                .setPatientAppointments(Arrays.asList(appointment))
                .setRoles(Arrays.asList(Role.PATIENT));

        appointment.setRegisteredPatient(true);
        appointment.setUnregisteredPatient(null);
        appointment.setPatient(patient);

        userRepository.save(patient);

        return userMapper.toOutbound(patient);
    }

    public File getPatientDownloadableReport(long id) {
        val report = getPatientReport(id);
        val context = new Context();
        val history = getPatientAppointmentHistory(id);

        val historyDtos = history.parallelStream()
                .map(app -> {
                    HistoryDto out = new HistoryDto();
                    out.setDoctor(app.getDoctor().getFirstName() + " " + app.getDoctor().getLastName());
                    StringBuilder assistances = new StringBuilder();
                    app.getAssistances().parallelStream().map(AssistanceDto::getName).forEach(a -> assistances.append(a).append(" "));
                    out.setAssistances(assistances.toString());
                    out.setTime(app.getTime().format(DateTimeFormatter.ISO_DATE));
                    out.setPrice(String.format("%.2f", app.getAssistances().parallelStream().mapToDouble(AssistanceDto::getPrice).sum()));
                    return out;
                })
                .collect(Collectors.toList());

        context.setVariable("report", report);
        context.setVariable("role", "Patient");

        if(historyDtos.isEmpty()) {
            val historyDto = new HistoryDto();
            historyDto.setPrice("0.00");
            historyDto.setTime("-");
            historyDto.setAssistances(" - ");
            historyDto.setDoctor(" - ");
            historyDtos.add(historyDto);
        }

        context.setVariable("history", historyDtos);
        pdfWriter.createPatientReport(context);
        return new File("report.pdf");
    }

    @Data
    public static class HistoryDto {
        private String doctor;
        private String time;
        private String assistances;
        private String price;
    }
}
