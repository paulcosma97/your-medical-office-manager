package ro.uvt.info.ymom.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Service;
import ro.uvt.info.ymom.entities.Appointment;
import ro.uvt.info.ymom.entities.embeddables.UnregisteredPatient;
import ro.uvt.info.ymom.exceptions.ForbiddenRequestException;
import ro.uvt.info.ymom.exceptions.InvalidRequestException;
import ro.uvt.info.ymom.exceptions.UserNotFoundException;
import ro.uvt.info.ymom.mappers.AppointmentMapper;
import ro.uvt.info.ymom.mappers.UnregisteredUserMapper;
import ro.uvt.info.ymom.models.AppointmentInDto;
import ro.uvt.info.ymom.models.AppointmentOutDto;
import ro.uvt.info.ymom.models.Role;
import ro.uvt.info.ymom.models.UnregisteredPatientInDto;
import ro.uvt.info.ymom.repositories.AppointmentRepository;
import ro.uvt.info.ymom.repositories.AssistanceRepository;
import ro.uvt.info.ymom.repositories.UserRepository;
import ro.uvt.info.ymom.utils.Utilitus;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class AppointmentService {
    private final AppointmentRepository appointmentRepository;
    private final UserRepository userRepository;
    private final AuthService authService;
    private final AppointmentMapper appointmentMapper;
    private final UnregisteredUserMapper unregisteredUserMapper;
    private final AssistanceRepository assistanceRepository;

    public List<AppointmentOutDto> getAppointments() {
        val user = authService.getAuthenticatedUser();

        if(user.getRoles().contains(Role.PATIENT)) {
            return user.getPatientAppointments().parallelStream().map(appointmentMapper::toOutbound).collect(Collectors.toList());
        } else if(user.getRoles().contains(Role.DOCTOR)) {
            return user.getDoctorAppointments().parallelStream().map(appointmentMapper::toOutbound).collect(Collectors.toList());
        }

        log.error("Could not get appointments for user {} because their roles {} don't include any of the following {}", user.getCnp(), user.getRoles(), new Role[] { Role.PATIENT, Role.DOCTOR });
        throw new InvalidRequestException("Users with role ADMIN can't have appointments.");
    }

    public AppointmentOutDto createAppointment(AppointmentInDto appointmentInDto) {
        validateAppointment(appointmentInDto);
        val appointment = new Appointment();
        addAppointmentData(appointment, appointmentInDto);

        return appointmentMapper.toOutbound(appointmentRepository.save(appointment));
    }

    public AppointmentOutDto updateAppointment(long id, AppointmentInDto appointmentInDto) {
        validateAppointment(appointmentInDto);
        val appointment = appointmentRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Could not find appointment"));
        if(appointment.getDoctor().getId() != authService.getAuthenticatedUser().getId()) {
            log.error("Doctor {} tried to edit doctor's {} appointment {}", authService.getAuthenticatedUser().getId(), appointment.getDoctor().getId(), appointment.getId());
            throw new ForbiddenRequestException("You can't update other doctor's appointments");
        }

        addAppointmentData(appointment, appointmentInDto);

        return appointmentMapper.toOutbound(appointmentRepository.save(appointment));
    }

    public void deleteAppointment(long id) {
        val appointment = appointmentRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Could not find appointment"));
        if(appointment.getDoctor().getId() != authService.getAuthenticatedUser().getId()) {
            log.error("Doctor {} tried to delete doctor's {} appointment {}", authService.getAuthenticatedUser().getId(), appointment.getDoctor().getId(), appointment.getId());
            throw new ForbiddenRequestException("You can't delete other doctor's appointments");
        }

        appointmentRepository.delete(appointment);
    }

    private void addAppointmentData(Appointment existing, AppointmentInDto appointment) {
        existing.setDoctor(authService.getAuthenticatedUser());
        if(appointment.isRegisteredPatient()) {
            existing.setPatient(userRepository.findById(appointment.getPatientId()).orElseThrow(UserNotFoundException::new));
            existing.setUnregisteredPatient(new UnregisteredPatient());
        } else {
            Utilitus.validateCnp(appointment.getUnregisteredPatient().getCnp());
            existing.setUnregisteredPatient(unregisteredUserMapper.toInbound(appointment.getUnregisteredPatient()));
        }
        existing.setRegisteredPatient(appointment.isRegisteredPatient());
        existing.setTime(appointment.getTime());

        val assistances = assistanceRepository.findAllById(appointment.getAssistanceIds());
        existing.setAssistances(assistances);
    }

    private void validateAppointment(AppointmentInDto appointment) {
        if(appointment.getTime().isBefore(LocalDateTime.now())) {
            log.error("Appointment has date in the past");
            throw new InvalidRequestException("Could not make an appointment for a date in the past");
        }

        if(!appointment.isRegisteredPatient() && !isValidUnregisteredPatient(appointment.getUnregisteredPatient())) {
            log.error("Appointment has unregistered patient flag checked but null patient data");
            throw new InvalidRequestException("Unregistered patient has invalid properties");
        } else if (appointment.isRegisteredPatient() && appointment.getPatientId() == 0) {
            log.error("Appointment has patient id 0");
            throw new InvalidRequestException("Appointment does not have a patient");
        }
    }

    private boolean isValidUnregisteredPatient(UnregisteredPatientInDto patient) {
        return patient != null &&
                patient.getFirstName() != null &&
                patient.getFirstName().length() >= 3 &&
                patient.getLastName() != null &&
                patient.getLastName().length() >= 3 &&
                patient.getCnp() != null &&
                patient.getCnp().length() == 13 &&
                containsOnlyDigits(patient.getCnp()) &&
                containsOnlyLettersAndSpaces(patient.getFirstName()) &&
                containsOnlyLetters(patient.getLastName());
    }

    private boolean containsOnlyDigits(String str) {
        return str.chars().allMatch(ch -> Character.isDigit((char)ch));
    }

    private boolean containsOnlyLettersAndSpaces(String str) {
        return str.chars().allMatch(ch -> Character.isLetter((char)ch) || (char)ch == ' ' );
    }

    private boolean containsOnlyLetters(String str) {
        return str.chars().allMatch(ch -> Character.isLetter((char)ch));
    }


    public AppointmentOutDto getAppointment(long id) {
        val user = authService.getAuthenticatedUser();
        val app = appointmentRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Could not find appointment"));

        if(!user.getDoctorAppointments().contains(app) && !user.getPatientAppointments().contains(app) && !user.getRoles().contains(Role.ADMIN)) {
            log.error("User {} attempted to get appointment {} which is not their.", user, app);
            throw new ForbiddenRequestException("You are not allowed to view this appointment");
        }

        return appointmentMapper.toOutbound(app);
    }
}
