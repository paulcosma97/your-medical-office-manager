package ro.uvt.info.ymom.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Service;
import ro.uvt.info.ymom.mappers.AssistanceMapper;
import ro.uvt.info.ymom.models.AssistanceDto;
import ro.uvt.info.ymom.repositories.AssistanceRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class AssistanceService {
    private final AssistanceRepository assistanceRepository;
    private final AssistanceMapper assistanceMapper;

    public List<AssistanceDto> getAssistances() {
        return assistanceRepository.findAll().parallelStream().map(assistanceMapper::toOutbound).collect(Collectors.toList());
    }

    public AssistanceDto getAssistance(long id) {
        return assistanceMapper.toOutbound(assistanceRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Could not find assistance")));
    }

    public AssistanceDto createAssistance(AssistanceDto assistanceDto) {
        return assistanceMapper.toOutbound(assistanceRepository.save(assistanceMapper.fromOutbound(assistanceDto)));
    }

    public AssistanceDto updateAssistance(long id, AssistanceDto assistanceDto) {
        val assistance = assistanceRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Could not find assistance"));
        val fromIn = assistanceMapper.fromOutbound(assistanceDto);

        assistance.setName(fromIn.getName());
        assistance.setPrice(fromIn.getPrice());

        return assistanceMapper.toOutbound(assistanceRepository.save(assistance));
    }

    public void deleteAssistance(long id) {
        assistanceRepository.delete(assistanceRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Could not find assistance")));

    }
}
