package ro.uvt.info.ymom.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ro.uvt.info.ymom.entities.User;
import ro.uvt.info.ymom.entities.embeddables.UnregisteredPatient;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppointmentOutDto {
    private long id;
    private User doctor;
    private User patient;
    private UnregisteredPatient unregisteredPatient;
    private boolean isRegisteredPatient;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime time;
    private List<AssistanceDto> assistances;
}
