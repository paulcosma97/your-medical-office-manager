package ro.uvt.info.ymom.mappers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ro.uvt.info.ymom.entities.Assistance;
import ro.uvt.info.ymom.exceptions.InvalidRequestException;
import ro.uvt.info.ymom.models.AssistanceDto;

import java.util.Collections;

@Slf4j
@RequiredArgsConstructor
public class AssistanceMapper {
    public AssistanceDto toOutbound(Assistance assistance) {
        return new AssistanceDto(assistance.getId(), assistance.getName(), assistance.getPrice());
    }

    public Assistance fromOutbound(AssistanceDto assistance) {
        if(assistance.getPrice() <= 0) {
            log.error("Assistance {} has property price with a negative value of {}", assistance, assistance.getPrice());
            throw new InvalidRequestException("Assistance has a negative price");
        }

        return new Assistance(0,
                assistance.getName(),
                assistance.getPrice(),
                Collections.emptyList()
            );
    }
}
