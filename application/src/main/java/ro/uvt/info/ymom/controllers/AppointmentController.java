package ro.uvt.info.ymom.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ro.uvt.info.ymom.models.AppointmentInDto;
import ro.uvt.info.ymom.models.AppointmentOutDto;
import ro.uvt.info.ymom.services.AppointmentService;
import ro.uvt.info.ymom.utils.CrudController;

import java.util.List;

@RestController
@RequestMapping("/appointments")
@RequiredArgsConstructor
public class AppointmentController implements CrudController<AppointmentInDto, AppointmentOutDto> {
    private final AppointmentService appointmentService;

    @GetMapping
    public List<AppointmentOutDto> getAll() {
        return appointmentService.getAppointments();
    }

    @GetMapping("/{id}")
    public AppointmentOutDto getOne(@PathVariable long id) {
        return appointmentService.getAppointment(id);
    }

    @PostMapping
    public AppointmentOutDto create(@RequestBody AppointmentInDto appointmentInDto) {
        return appointmentService.createAppointment(appointmentInDto);
    }

    @PutMapping("/{id}")
    public AppointmentOutDto update(@PathVariable long id, @RequestBody AppointmentInDto appointmentInDto) {
        return appointmentService.updateAppointment(id, appointmentInDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        appointmentService.deleteAppointment(id);
    }
}
