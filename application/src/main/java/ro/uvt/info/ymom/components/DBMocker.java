package ro.uvt.info.ymom.components;

import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import ro.uvt.info.ymom.entities.Assistance;
import ro.uvt.info.ymom.entities.User;
import ro.uvt.info.ymom.models.Role;
import ro.uvt.info.ymom.repositories.AssistanceRepository;
import ro.uvt.info.ymom.repositories.UserRepository;

import java.time.LocalDate;
import java.util.Arrays;

@Profile("dev")
@Component
@RequiredArgsConstructor
public class DBMocker implements CommandLineRunner {
    private final UserRepository userRepository;
    private final AssistanceRepository assistanceRepository;

    @Override
    public void run(String... args) throws Exception {
        createUsers();
        createAssistances();
    }

    private void createAssistances() {
        assistanceRepository.save(new Assistance().setName("Vision Check").setPrice(74.59));
        assistanceRepository.save(new Assistance().setName("General Examination").setPrice(34.99));
        assistanceRepository.save(new Assistance().setName("Dental Check").setPrice(100.00));
    }

    private void createUsers() {
        val pass = "$2a$10$9SceenE19f27J8RRHObr2.60cuavP/C8gbBNkc4U1qey9jZEr3NV2";

        userRepository.save(new User()
                .setFirstName("Admin")
                .setLastName("Adminescu")
                .setPassword(pass)
                .setCnp("1970508245039")
                .setBirthday(LocalDate.of(1997, 5, 8))
                .setRoles(Arrays.asList(Role.ADMIN))
        );

        userRepository.save(new User()
                .setFirstName("Doctor")
                .setLastName("Doctorescu")
                .setPassword(pass)
                .setCnp("1970508245038")
                .setBirthday(LocalDate.of(1997, 5, 8))
                .setRoles(Arrays.asList(Role.DOCTOR))
        );

        userRepository.save(new User()
                .setFirstName("Patient")
                .setLastName("Patientescu")
                .setPassword(pass)
                .setCnp("1970508245037")
                .setBirthday(LocalDate.of(1997, 5, 8))
                .setRoles(Arrays.asList(Role.PATIENT))
        );
    }
}
