package ro.uvt.info.ymom.mappers;

import lombok.RequiredArgsConstructor;
import ro.uvt.info.ymom.entities.embeddables.UnregisteredPatient;
import ro.uvt.info.ymom.models.UnregisteredPatientInDto;
import ro.uvt.info.ymom.utils.Utilitus;

@RequiredArgsConstructor
public class UnregisteredUserMapper {


    public UnregisteredPatient toInbound(UnregisteredPatientInDto unregisteredPatientInDto) {
        return new UnregisteredPatient(unregisteredPatientInDto.getFirstName(), unregisteredPatientInDto.getLastName(), unregisteredPatientInDto.getCnp(), Utilitus.getBirthDateFromCnp(unregisteredPatientInDto.getCnp()));
    }
}
