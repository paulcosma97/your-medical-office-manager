package ro.uvt.info.ymom.mappers;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import ro.uvt.info.ymom.entities.User;
import ro.uvt.info.ymom.models.UserInDto;
import ro.uvt.info.ymom.models.UserOutDto;
import ro.uvt.info.ymom.utils.Utilitus;

import java.util.Arrays;
import java.util.Collections;

@RequiredArgsConstructor
public class UserMapper {
    private final PasswordEncoder passwordEncoder;

    public UserOutDto toOutbound(User user) {
        return new UserOutDto(user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getCnp(),
                user.getRoles(),
                user.getBirthday()
        );
    }

    public User fromOutbound(UserInDto user) {
        return new User(0,
                user.getFirstName(),
                user.getLastName(),
                passwordEncoder.encode(user.getPassword()),
                user.getCnp(),
                Arrays.asList(),
                Utilitus.getBirthDateFromCnp(user.getCnp()),
                Arrays.asList(),
                Arrays.asList()
        );
    }
}
