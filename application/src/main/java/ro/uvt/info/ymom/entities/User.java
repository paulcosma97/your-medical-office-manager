package ro.uvt.info.ymom.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.Accessors;
import ro.uvt.info.ymom.models.Role;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
@ToString(of = "id")
@NoArgsConstructor
@Accessors(chain = true)
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(length = 50, nullable = false)
    private String firstName;
    @Column(length = 50, nullable = false)
    private String lastName;
    @JsonIgnore
    @NotNull
    private String password;
    @Column(length = 13, nullable = false, unique = true)
    private String cnp;
    @Enumerated(EnumType.STRING)
    @ElementCollection(fetch = FetchType.EAGER)
    private List<Role> roles;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;
    @JsonIgnore
    @OneToMany(mappedBy = "patient")
    private List<Appointment> patientAppointments;
    @JsonIgnore
    @OneToMany(mappedBy = "doctor")
    private List<Appointment> doctorAppointments;

}
