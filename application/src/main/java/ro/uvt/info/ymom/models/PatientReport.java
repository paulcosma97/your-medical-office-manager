package ro.uvt.info.ymom.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PatientReport {
    private UserOutDto user;
    private double totalAmount;
    private double lastYear;
    private double lastMonth;
    private double lastDay;
}
