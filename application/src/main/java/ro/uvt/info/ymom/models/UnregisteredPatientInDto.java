package ro.uvt.info.ymom.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnregisteredPatientInDto {
    private String firstName;
    private String lastName;
    private String cnp;
}
