package ro.uvt.info.ymom.exceptions;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException() {
        super("Could not find requested user");
    }
}
