package ro.uvt.info.ymom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ro.uvt.info.ymom.entities.User;
import ro.uvt.info.ymom.models.Role;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByCnp(String cnp);
    List<User> findAllByRolesContaining(Role role);
    @Query("select max(user.id) from User user")
    Long findLastId();
}
