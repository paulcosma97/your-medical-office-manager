package ro.uvt.info.ymom.components;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ro.uvt.info.ymom.exceptions.ForbiddenRequestException;
import ro.uvt.info.ymom.exceptions.InvalidRequestException;
import ro.uvt.info.ymom.exceptions.UserNotFoundException;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;

@RestControllerAdvice
public class RestExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(InvalidRequestException.class)
    public ErrorMessage handleInvalidRequestException(Exception e) {
        return ErrorMessage.of(e, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler({UserNotFoundException.class, EntityNotFoundException.class})
    public ErrorMessage handleNotFoundException(Exception e) {
        return ErrorMessage.of(e, HttpStatus.NOT_FOUND);
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(ForbiddenRequestException.class)
    public ErrorMessage handleForbiddenRequestException(Exception e) {
        return ErrorMessage.of(e, HttpStatus.FORBIDDEN);
    }

    @Data
    @NoArgsConstructor
    private static class ErrorMessage {
        String message;
        String error;
        int status;
        LocalDateTime timestamp;

        static ErrorMessage of(Exception e, HttpStatus status) {
            val error = new ErrorMessage();
            error.setMessage(e.getMessage());
            error.setError(e.getMessage());
            error.setTimestamp(LocalDateTime.now());
            error.setStatus(status.value());
            return error;
        }
    }
}
