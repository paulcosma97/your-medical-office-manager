package ro.uvt.info.ymom.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ro.uvt.info.ymom.entities.User;
import ro.uvt.info.ymom.exceptions.InvalidRequestException;
import ro.uvt.info.ymom.exceptions.UserNotFoundException;
import ro.uvt.info.ymom.mappers.UserMapper;
import ro.uvt.info.ymom.models.AuthCredentials;
import ro.uvt.info.ymom.models.UserOutDto;
import ro.uvt.info.ymom.repositories.UserRepository;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;

    public UserOutDto canLogin(AuthCredentials authCredentials) {
        if (authCredentials.getCnp() == null || authCredentials.getPassword() == null) {
            log.error("Authentication credentials {} have null properties", authCredentials);
            throw new InvalidRequestException("User CNP or password is empty");
        }

        val found = userRepository.findByCnp(authCredentials.getCnp()).orElseThrow(UserNotFoundException::new);
        if (!passwordEncoder.matches(authCredentials.getPassword(), found.getPassword())) {
            throw new UserNotFoundException();
        }

        return userMapper.toOutbound(found);
    }

    public User getAuthenticatedUser() {
        return userRepository.findByCnp(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(UserNotFoundException::new);
    }


}
