package ro.uvt.info.ymom.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserOutDto {
    private long id;
    private String firstName;
    private String lastName;
    private String cnp;
    private List<Role> roles;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;
}
