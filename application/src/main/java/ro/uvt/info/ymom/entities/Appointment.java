package ro.uvt.info.ymom.entities;

import lombok.*;
import ro.uvt.info.ymom.entities.embeddables.UnregisteredPatient;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@ToString(of = "id")
public class Appointment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    private User doctor;
    @ManyToOne
    private User patient;
    @Embedded
    private UnregisteredPatient unregisteredPatient;
    private boolean isRegisteredPatient;
    private LocalDateTime time;
    @ManyToMany
    private List<Assistance> assistances;
}
