package ro.uvt.info.ymom.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.*;
import ro.uvt.info.ymom.models.*;
import ro.uvt.info.ymom.services.UserService;
import ro.uvt.info.ymom.utils.CrudController;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
@CrossOrigin
public class PatientController implements CrudController<UserInDto, UserOutDto> {
    private final UserService userService;

    @GetMapping
    public List<UserOutDto> getAll() {
        return userService.getUsers(Role.PATIENT);
    }

    @GetMapping("/{id}")
    public UserOutDto getOne(@PathVariable long id) {
        return userService.getUser(id);
    }

    @GetMapping("/cnp/{cnp}")
    public UserOutDto getOne(@PathVariable String cnp) {
        return userService.getUserByCnp(cnp);
    }

    @GetMapping("/{id}/history")
    public List<AppointmentOutDto> getAppointmentHistory(@PathVariable long id) {
        return userService.getPatientAppointmentHistory(id);
    }

    @GetMapping("/{id}/report")
    public PatientReport getReport(@PathVariable long id) {
        return userService.getPatientReport(id);
    }

    @GetMapping(path = "/{id}/report/download", produces = "application/pdf")
    public FileSystemResource getDownloadableReport(@PathVariable long id) {
        return new FileSystemResource(userService.getPatientDownloadableReport(id));
    }

    @GetMapping("/report")
    public List<PatientReport> getAllReports() {
        return userService.getAllPatientReports();
    }

    @PostMapping
    public UserOutDto create(@RequestBody UserInDto userInDto) {
        return userService.createUser(userInDto, Role.PATIENT);
    }

    @PostMapping("/save-unregistered/{appId}")
    public UserOutDto saveUnregistered(@PathVariable long appId, @RequestBody UserInDto userInDto) {
        return userService.saveUnregistered(appId, userInDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        userService.deleteUser(id);
    }

    @PutMapping("/{id}")
    public UserOutDto update(@PathVariable long id, @RequestBody UserInDto userInDto) {
        return userService.updateUser(id, userInDto);
    }



}
