package ro.uvt.info.ymom.configurations;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;
import ro.uvt.info.ymom.mappers.AppointmentMapper;
import ro.uvt.info.ymom.mappers.AssistanceMapper;
import ro.uvt.info.ymom.mappers.UnregisteredUserMapper;
import ro.uvt.info.ymom.mappers.UserMapper;

@Configuration
@RequiredArgsConstructor
public class MapperConfiguration {
    private final PasswordEncoder passwordEncoder;

    @Bean
    public AppointmentMapper appointmentMapper() {
        return new AppointmentMapper(assistanceMapper());
    }

    @Bean
    public UnregisteredUserMapper unregisteredUserMapper() {
        return new UnregisteredUserMapper();
    }

    @Bean
    public AssistanceMapper assistanceMapper() {
        return new AssistanceMapper();
    }

    @Bean
    public UserMapper userMapper() {
        return new UserMapper(passwordEncoder);
    }
}
