package ro.uvt.info.ymom.utils;

import java.util.List;

public interface CrudController<I, O> {
    List<O> getAll();

    O getOne(long id);

    O create(I inDto);

    O update(long id, I inDto);

    void delete(long id);
}
