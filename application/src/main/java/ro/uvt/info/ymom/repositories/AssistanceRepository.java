package ro.uvt.info.ymom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.uvt.info.ymom.entities.Assistance;

public interface AssistanceRepository extends JpaRepository<Assistance, Long> {
}
