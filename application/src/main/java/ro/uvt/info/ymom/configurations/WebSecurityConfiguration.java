package ro.uvt.info.ymom.configurations;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import ro.uvt.info.ymom.entities.User;
import ro.uvt.info.ymom.models.Role;
import ro.uvt.info.ymom.repositories.UserRepository;

import javax.persistence.EntityNotFoundException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.stream.Collectors;

@Slf4j
@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final UserRepository userRepository;
    @Value("${ymom.security.cross-origins:}")
    private String[] crossOrigins;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(cnp -> new UserDetailsImpl(userRepository.findByCnp(cnp).orElseThrow(EntityNotFoundException::new)));
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                /* Misc */
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers("/", "/login").permitAll()
                .antMatchers("/profile").authenticated()

                /* Appointments */
                .antMatchers(HttpMethod.GET, "/appointments").authenticated()
                .antMatchers(HttpMethod.POST, "/appointments").hasAnyRole(Role.DOCTOR.name(), Role.ADMIN.name())
                .antMatchers(HttpMethod.PUT, "/appointments").hasAnyRole(Role.DOCTOR.name(), Role.ADMIN.name())
                .antMatchers(HttpMethod.DELETE, "/appointments").hasAnyRole(Role.DOCTOR.name(), Role.ADMIN.name())
                .antMatchers(HttpMethod.OPTIONS, "/appointments").permitAll()

                /* Assistances */
                .antMatchers(HttpMethod.GET, "/assistances").authenticated()
                .antMatchers(HttpMethod.POST, "/assistances").hasAnyRole(Role.DOCTOR.name(), Role.ADMIN.name())
                .antMatchers(HttpMethod.PUT, "/assistances").hasAnyRole(Role.DOCTOR.name(), Role.ADMIN.name())
                .antMatchers(HttpMethod.DELETE, "/assistances").hasAnyRole(Role.DOCTOR.name(), Role.ADMIN.name())
                .antMatchers(HttpMethod.OPTIONS, "/assistances").permitAll()

                /* Patients */
                .antMatchers("/users").hasAnyRole(Role.DOCTOR.name(), Role.ADMIN.name())
                .antMatchers(HttpMethod.OPTIONS, "/users").permitAll()


                /* Doctors */
                .antMatchers("/doctors").hasRole(Role.ADMIN.name())
                .antMatchers(HttpMethod.OPTIONS, "/doctors").permitAll()

                .and()
            .httpBasic().and()
            .csrf().disable()
            .headers().frameOptions().disable().and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @RequiredArgsConstructor
    public static class UserDetailsImpl implements UserDetails {
        final User user;

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return user.getRoles().parallelStream()
                    .map(Role::roleName)
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());
        }

        @Override
        public String getPassword() {
            return user.getPassword();
        }

        @Override
        public String getUsername() {
            return user.getCnp();
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins(crossOrigins)
                        .allowedMethods("*")
                        .allowedHeaders("*");
            }
        };
    }
}