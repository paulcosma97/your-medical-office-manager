package ro.uvt.info.ymom.mappers;

import lombok.RequiredArgsConstructor;
import ro.uvt.info.ymom.entities.Appointment;
import ro.uvt.info.ymom.models.AppointmentOutDto;

import java.util.stream.Collectors;

@RequiredArgsConstructor
public class AppointmentMapper {
    private final AssistanceMapper assistanceMapper;

    public AppointmentOutDto toOutbound(Appointment appointment) {
        return new AppointmentOutDto(appointment.getId(), appointment.getDoctor(), appointment.getPatient(), appointment.getUnregisteredPatient(), appointment.isRegisteredPatient(), appointment.getTime(), appointment.getAssistances().parallelStream().map(assistanceMapper::toOutbound).collect(Collectors.toList()));
    }


}
