package ro.uvt.info.ymom.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AssistanceDto {
    private long id;
    private String name;
    private double price;
}
