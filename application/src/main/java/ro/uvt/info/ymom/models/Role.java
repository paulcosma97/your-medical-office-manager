package ro.uvt.info.ymom.models;

public enum Role {
    PATIENT, DOCTOR, ADMIN;

    public String roleName() {
        return "ROLE_" + this.name();
    }

}
