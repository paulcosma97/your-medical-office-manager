package ro.uvt.info.ymom.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ro.uvt.info.ymom.models.AssistanceDto;
import ro.uvt.info.ymom.services.AssistanceService;
import ro.uvt.info.ymom.utils.CrudController;

import java.util.List;

@RestController
@RequestMapping("/assistances")
@RequiredArgsConstructor
public class AssistanceController implements CrudController<AssistanceDto, AssistanceDto> {
    private final AssistanceService assistanceService;

    @GetMapping
    public List<AssistanceDto> getAll() {
        return assistanceService.getAssistances();
    }

    @GetMapping("/{id}")
    public AssistanceDto getOne(@PathVariable long id) {
        return assistanceService.getAssistance(id);
    }

    @PostMapping
    public AssistanceDto create(@RequestBody AssistanceDto appointmentDto) {
        return assistanceService.createAssistance(appointmentDto);
    }

    @PutMapping("/{id}")
    public AssistanceDto update(@PathVariable long id, @RequestBody AssistanceDto appointmentDto) {
        return assistanceService.updateAssistance(id, appointmentDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        assistanceService.deleteAssistance(id);
    }
}
