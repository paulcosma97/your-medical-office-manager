package ro.uvt.info.ymom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.uvt.info.ymom.entities.Appointment;
import ro.uvt.info.ymom.entities.User;

import java.time.LocalDateTime;
import java.util.List;

public interface AppointmentRepository extends JpaRepository<Appointment, Long> {
    List<Appointment> findAllByDoctorAndTimeAfterOrderByTimeAsc(User doctor, LocalDateTime time);

    default List<Appointment> findAllUpcomingByDoctor(User doctor) {
        return findAllByDoctorAndTimeAfterOrderByTimeAsc(doctor, LocalDateTime.now());
    }
}
