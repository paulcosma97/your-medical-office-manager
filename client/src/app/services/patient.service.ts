import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {LogService} from "./log.service";
import {Report, User} from "../models/user.model";
import {Appointment} from "../models/appointment.model";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  constructor(private http: HttpClient,
              private router: Router,
              private log: LogService) {
  }

  getByCnp(cnp: string, fnSuccess, fnError) {
    this.http.get('http://localhost:8080/users/cnp/' + cnp)
      .subscribe(((p: User) => fnSuccess(p)), fnError);
  }

  getAll(fnSuccess, fnError) {
    this.http.get('http://localhost:8080/users')
      .subscribe(((p: User[]) => fnSuccess(p)), fnError);
  }

  getHistory(id, fnSuccess, fnError) {
    this.http.get('http://localhost:8080/users/' + id + '/history')
      .subscribe(((p: Appointment[]) => fnSuccess(p)), fnError);
  }

  saveUnregistered(appId: number, password: string) {
    this.http.post('http://localhost:8080/users/save-unregistered/' + appId, { password: password }).subscribe(() => {
      this.router.navigate(['/appointment/' + appId]);
      this.log.info("Patient is now registered");
    }, (error: HttpErrorResponse) => this.log.error(error.message, error))
  }

  getReport(id, fnSuccess) {
    this.http.get('http://localhost:8080/users/' + id + '/report').subscribe((result: Report) => fnSuccess(result),
      (error: HttpErrorResponse) => this.log.error(error.message, error))
  }

  delete(id: number) {
    this.http.delete('http://localhost:8080/users/' + id)
      .subscribe(() => {}, (error: HttpErrorResponse) => this.log.error(error.message, error));
  }
}
