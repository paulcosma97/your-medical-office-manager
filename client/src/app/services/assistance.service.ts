import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Assistance} from "../models/assistance.model";
import {LogService} from "./log.service";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AssistanceService {

  constructor(private http: HttpClient,
              private log: LogService,
              private router: Router
  ) { }

  getAll(success: Function) {
    this.http.get('http://localhost:8080/assistances').subscribe((result: Assistance[]) => {
      success(result)
    }, error => this.log.error('Could not retrieve assistances', error));
  }

  getOne(id: any, fnSuccess: (assistance: Assistance) => Assistance) {
    this.http.get('http://localhost:8080/assistances/' + id).subscribe((result: Assistance) => {
      fnSuccess(result)
    }, error => this.log.error('Could not retrieve assistance', error));
  }

  update(id: any, assistance: Assistance) {
    this.http.put('http://localhost:8080/assistances/' + id, assistance).subscribe(
      () => this.log.info("Assistance updated"),
      error => this.log.error('Could not save assistance', error)
    )
  }

  delete(id: any) {
    this.http.delete('http://localhost:8080/assistances/' + id).subscribe(
      () => {
        this.log.info("Assistance removed");
        this.router.navigate(['/assistance']);
      },
      error => this.log.error('Could not remove assistance', error)
    )
  }

  save(assistanceToSave: Assistance) {
    this.http.post('http://localhost:8080/assistances/', assistanceToSave).subscribe(
      (assistance: Assistance) => {
        this.log.info("Assistance created!");
        this.router.navigate(['/assistance/' + assistance.id]);
      },
      error => this.log.error('Could not create assistance', error)
    )
  }
}
