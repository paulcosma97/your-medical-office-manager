import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {LogService} from './log.service';
import {User} from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient,
              private router: Router,
              private log: LogService
  ) { }

  login(credentials: any) {
    this.http.post('http://localhost:8080/login', credentials).subscribe((data: any) => {
        this.log.info('You successfully logged in!');
        data['password'] = credentials['password'];
        localStorage.setItem('user', JSON.stringify(data));
        this.router.navigate(['home']);
      },
      (error) => {
        this.log.error('Could not log in!', error);
      });
  }

  getAuthenticatedUser(): User {
    return <User> JSON.parse(localStorage.getItem('user'));
  }

  isAuthenticated(): boolean {
    return !!localStorage.getItem('user');
  }

  logout() {
    localStorage.removeItem('user');
    this.router.navigate(['login']);
  }
}
