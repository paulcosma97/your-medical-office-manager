import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";
import {LogService} from "./log.service";
import {User} from "../models/user.model";

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  constructor(private http: HttpClient,
              private router: Router,
              private log: LogService) {
  }

  getAll(fnSuccess) {
    this.http.get('http://localhost:8080/doctors')
      .subscribe(((p: User[]) => fnSuccess(p)), (error: HttpErrorResponse) => this.log.error(error.message, error));
  }

  delete(id: number) {
    this.http.delete('http://localhost:8080/doctors/' + id)
      .subscribe(() => {}, (error: HttpErrorResponse) => this.log.error(error.message, error));
  }

  create(user: User) {
    this.http.post('http://localhost:8080/doctors', user)
      .subscribe(() => {
        window.location.reload(true);
      }, (error: HttpErrorResponse) => this.log.error('Could not create doctor!', error));
  }
}
