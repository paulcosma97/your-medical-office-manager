import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Appointment} from "../models/appointment.model";
import {LogService} from "./log.service";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {

  constructor(private http: HttpClient,
              private log: LogService,
              private router: Router) { }

  getAll(success: Function) {
    this.http.get('http://localhost:8080/appointments').subscribe( (data: Appointment[]) => {
      success(data)
    }, (error => this.log.error('Could not retrieve appointments', error)))
  }

  getOne(id: any, success: Function) {
    this.http.get('http://localhost:8080/appointments/' + id).subscribe( (data: Appointment) => {
      success(data)
    }, (error => {
      this.log.error('Could not retrieve appointment ' + id, error);
      this.router.navigate([''])
    }))
  }

  remove(id: number) {
    this.http.delete('http://localhost:8080/appointments/' + id).subscribe( () => {
      this.log.info('Successfully removed appointment ' + id);
      this.router.navigate(['/appointment'])
    }, (error => {
      this.log.error('Could not delete appointment ' + id, error);
    }))
  }

  save(data: any, fnSuccess: Function) {
    this.http.post('http://localhost:8080/appointments', data)
      .subscribe((result: Appointment) => {
          this.log.info('Successfully saved the appointment');
          fnSuccess(result)
        },
        (response: HttpErrorResponse) => this.log.error(response.error.message, response))
  }
}
