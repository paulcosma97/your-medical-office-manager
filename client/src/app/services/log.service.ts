import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {NotificationOptions} from '../components/notification/notification.component';

@Injectable({
  providedIn: 'root'
})
export class LogService {
  private notificationOptions: NotificationOptions = {
    title: 'def',
    body: 'def',
    style: 'none'
  };
  private hasChanged = new BehaviorSubject(this.notificationOptions);
  onChange = this.hasChanged.asObservable();

  constructor() {
  }

  error(msg: string, ...elements: any[]) {
    console.warn(msg, elements);
    this.hasChanged.next(<NotificationOptions> {
      title: 'Oh boy :(',
      body: msg,
      style: 'error'
    });
  }

  debug(msg: string, ...elements: any[]) {
    console.log(msg, elements);
  }

  info(msg: string) {
    this.hasChanged.next({
      title: 'Woohoo',
      body: msg,
      style: 'info'
    });
  }
}
