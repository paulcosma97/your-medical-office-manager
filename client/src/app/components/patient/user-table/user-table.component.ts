import {Component, Input, OnInit} from '@angular/core';
import {User} from "../../../models/user.model";
import {Appointment} from "../../../models/appointment.model";
import {AuthService} from "../../../services/auth.service";
import {DoctorService} from "../../../services/doctor.service";
import {PatientService} from "../../../services/patient.service";

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent implements OnInit {
  @Input() users: User[] = [];
  @Input() type: string = "user";
  searchFilter: string = "";
  loggedUser: User;

  constructor(
    private authService: AuthService,
    private doctorService: DoctorService,
    private patientService: PatientService
  ) { }

  ngOnInit() {
    this.loggedUser = this.authService.getAuthenticatedUser();
  }

  filter(users: User[]): User[] {
    return users.filter(user => {
        return user.lastName.toLocaleLowerCase().includes(this.searchFilter.toLocaleLowerCase()) ||
          user.firstName.toLocaleLowerCase().includes(this.searchFilter.toLocaleLowerCase()) ||
          user.cnp.toLocaleLowerCase().includes(this.searchFilter.toLocaleLowerCase());
    })
  }

  removeUser(user: User) {
    if(user.roles.includes('DOCTOR')) {
      this.doctorService.delete(user.id);
    } else {
      this.patientService.delete(user.id);
    }

    this.users = this.users.filter(u => u != user);
  }
}
