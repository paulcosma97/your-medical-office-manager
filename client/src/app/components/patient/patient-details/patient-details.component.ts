import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AppointmentService} from "../../../services/appointment.service";
import {PatientService} from "../../../services/patient.service";
import {Appointment} from "../../../models/appointment.model";
import {Report, User} from "../../../models/user.model";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {LogService} from "../../../services/log.service";
import {AuthService} from "../../../services/auth.service";
import {RequestOptions, ResponseContentType} from "@angular/http";

@Component({
  selector: 'app-user-details',
  templateUrl: './patient-details.component.html',
  styleUrls: ['./patient-details.component.css']
})
export class PatientDetailsComponent implements OnInit {
  patient: User;
  appointmentHistory: Appointment[] = [];
  report: Report;
  loggedUser: User;

  constructor(private route: ActivatedRoute,
              private patientService: PatientService,
              private authService: AuthService,
              private http: HttpClient,
              private log: LogService
  ) { }

  ngOnInit() {
    this.loggedUser = this.authService.getAuthenticatedUser();

    this.route.params.subscribe(data => this.patientService.getByCnp(data.id,
      (result: User) => {
      this.patient = result;
      this.afterPatientLoaded();
      },
      (error: HttpErrorResponse) => this.log.error(error.message, error)));

  }

  afterPatientLoaded() {
    this.patientService.getHistory(this.patient.id, (apps: Appointment[]) => this.appointmentHistory = apps, (error: HttpErrorResponse) => this.log.error(error.message, error));
    this.patientService.getReport(this.patient.id, (report: Report) => this.report = report);
  }

  download() {

    this.http.get(`http://localhost:8080/users/${this.patient.id}/report/download`, {responseType: 'blob'}).subscribe(data => {
      const blob = new Blob([data], { type: 'application/pdf'});
      const url = window.URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = url;
      link.click();
    })
  }

}
