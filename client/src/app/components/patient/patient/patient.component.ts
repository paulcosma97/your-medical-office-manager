import { Component, OnInit } from '@angular/core';
import {User} from "../../../models/user.model";
import {PatientService} from "../../../services/patient.service";
import {LogService} from "../../../services/log.service";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {
  patients: User[] = [];

  constructor(
    private patientService: PatientService,
    private log: LogService
  ) { }

  ngOnInit() {
    this.patientService.getAll((patients: User[]) => this.patients = patients, (error: HttpErrorResponse) => this.log.error(error.message, error));
  }

}
