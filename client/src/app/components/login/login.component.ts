import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private auth: AuthService) { }

  @ViewChild('loginForm') loginForm: NgForm;

  ngOnInit(): void {
    console.log(this.loginForm);
  }

  verifyAccount() {
    this.auth.login(this.loginForm.value);
  }

}
