import { Component, OnInit } from '@angular/core';
import {LogService} from '../../services/log.service';
import {AuthService} from '../../services/auth.service';
import {User} from '../../models/user.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  loggedUser: User;

  constructor(private log: LogService,
              private auth: AuthService
  ) { }

  ngOnInit() {
    this.loggedUser = this.auth.getAuthenticatedUser();
  }
}
