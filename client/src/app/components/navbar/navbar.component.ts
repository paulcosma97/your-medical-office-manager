import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {User} from "../../models/user.model";
import {DoctorService} from "../../services/doctor.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  loggedUser: User;
  @ViewChild('modalSave') modalSave: ElementRef;

  cnp: string = '';
  firstName: string = '';
  lastName: string = '';
  password: string = '';


  constructor(private authService: AuthService,
              private doctorService: DoctorService
  ) { }

  ngOnInit() {
    this.loggedUser = this.authService.getAuthenticatedUser();
  }

  logout() {
    this.authService.logout();
  }

  openCreateDoctorModal() {
    this.modalSave.nativeElement.style.display = 'block';
  }

  closeCreateDoctorModal() {
    this.modalSave.nativeElement.style.display = 'none';
  }

  createDoctor() {
    const doctor = new User();
    doctor.lastName = this.lastName;
    doctor.firstName = this.firstName;
    doctor.cnp = this.cnp;
    doctor.password = this.password;
    this.doctorService.create(doctor);

    this.closeCreateDoctorModal();
  }
}
