import { Component, OnInit } from '@angular/core';
import {LogService} from '../../services/log.service';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: [ './notification.component.css' ],
  animations: [
    trigger('onClose', [
      state('visible', style({
        opacity: 1
      })),
      state('invisible', style({
        opacity: 0
      })),
      transition('visible <=> invisible', [
        animate('0.3s')
      ])
    ])
  ]
})
export class NotificationComponent implements OnInit {
  title: string;
  body: string;
  style: string;
  isVisible: boolean;
  animationBegin: boolean;

  constructor(private log: LogService) {
  }

  ngOnInit() {
    this.log.onChange.subscribe((options) => this.show(options));
  }

  show(options: NotificationOptions) {
    this.title = options.title;
    this.body = options.body;
    this.style = 'app-' + options.style;
    this.isVisible = options.style !== 'none';

    setTimeout(() => {
      this.isVisible = false;
      this.animationBegin = false;
    }, 3000);

    setTimeout(() => this.animationBegin = true, 2700);
  }

}

export class NotificationOptions {
  title: string;
  body: string;
  style: string;
}
