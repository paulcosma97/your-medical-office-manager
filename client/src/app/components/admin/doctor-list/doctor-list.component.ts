import { Component, OnInit } from '@angular/core';
import {LogService} from "../../../services/log.service";
import {DoctorService} from "../../../services/doctor.service";
import {User} from "../../../models/user.model";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-doctor-list',
  templateUrl: './doctor-list.component.html',
  styleUrls: ['./doctor-list.component.css']
})
export class DoctorListComponent implements OnInit {
  doctors: User[] = [];

  constructor(private log: LogService,
              private doctorService: DoctorService
  ) { }

  ngOnInit() {
    this.doctorService.getAll((users: User[]) => this.doctors = users);
  }

}
