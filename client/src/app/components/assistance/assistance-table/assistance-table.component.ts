import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {Assistance} from "../../../models/assistance.model";
import {AssistanceService} from "../../../services/assistance.service";
import {LogService} from "../../../services/log.service";

@Component({
  selector: 'app-assistance-table',
  templateUrl: './assistance-table.component.html',
  styleUrls: ['./assistance-table.component.css']
})
export class AssistanceTableComponent implements OnInit {
  @ViewChild('modalSave') modalSave: ElementRef;
  @Input() assistances: Assistance[];
  @Input() canCRUD: boolean = false;

  assistanceToSave: Assistance = new Assistance();

  constructor(
    private assService: AssistanceService,
    private log: LogService
  ) { }

  ngOnInit() {
  }

  showModal() {
    this.modalSave.nativeElement.style.display = 'block';
  }

  closeModal() {
    this.modalSave.nativeElement.style.display = 'none';
  }

  save() {
    if(this.assistanceToSave.name.length < 3 || this.assistanceToSave.price < 0) {
      return this.log.error("Please add valid data");
    }

    this.assService.save(this.assistanceToSave);
  }
}
