import { Component, OnInit } from '@angular/core';
import {Assistance} from "../../../models/assistance.model";
import {AssistanceService} from "../../../services/assistance.service";
import {ActivatedRoute, Router} from "@angular/router";
import {LogService} from "../../../services/log.service";

@Component({
  selector: 'app-assistance-details',
  templateUrl: './assistance-details.component.html',
  styleUrls: ['./assistance-details.component.css']
})
export class AssistanceDetailsComponent implements OnInit {
  assistance: Assistance;
  constructor(
    private assService: AssistanceService,
    private router: ActivatedRoute,
    private log: LogService
  ) { }

  ngOnInit() {
    this.router.params.subscribe(data => this.assService.getOne(data.id, (assistance: Assistance) => this.assistance = assistance));
  }

  saveAssistance() {
    if(this.assistance.name.length < 3 || this.assistance.price < 0) {
      return this.log.error("Please add valid data");
    }

    this.assService.update(this.assistance.id, this.assistance);
  }

  removeAssistance() {
    this.assService.delete(this.assistance.id);
  }

}
