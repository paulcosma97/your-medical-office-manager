import { Component, OnInit } from '@angular/core';
import {AssistanceService} from "../../../services/assistance.service";
import {Assistance} from "../../../models/assistance.model";
import {User} from "../../../models/user.model";
import {AuthService} from "../../../services/auth.service";

@Component({
  selector: 'app-assistance',
  templateUrl: './assistance.component.html',
  styleUrls: ['./assistance.component.css']
})
export class AssistanceComponent implements OnInit {
  assistances: Assistance[] = [];
  loggedUser: User;

  constructor(
    private assService: AssistanceService,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.assService.getAll((assistances: Assistance[]) => this.assistances = assistances);
    this.loggedUser = this.authService.getAuthenticatedUser();
  }

}
