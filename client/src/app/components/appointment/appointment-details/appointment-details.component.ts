import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Appointment} from "../../../models/appointment.model";
import {AppointmentService} from "../../../services/appointment.service";
import {PatientService} from "../../../services/patient.service";
import {AuthService} from "../../../services/auth.service";
import {User} from "../../../models/user.model";

@Component({
  selector: 'app-appointment-details',
  templateUrl: './appointment-details.component.html',
  styleUrls: ['./appointment-details.component.css']
})
export class AppointmentDetailsComponent implements OnInit {
  appointment: Appointment;
  password: string = "";
  loggedUser: User;
  @ViewChild('modal') modal: ElementRef;
  @ViewChild('modalSave') modalSave: ElementRef;

  constructor(private route: ActivatedRoute,
              private appService: AppointmentService,
              private patientService: PatientService,
              private authService: AuthService
              ) { }

  ngOnInit() {
    this.loggedUser = this.authService.getAuthenticatedUser();
    this.route.params.subscribe(data => this.appService.getOne(data.id, (result: Appointment) => this.appointment = result))
  }

  showModal() {
    this.modal.nativeElement.style.display = 'block';
  }

  closeModal() {
    this.modal.nativeElement.style.display = 'none';
  }

  removeAppointment() {
    this.appService.remove(this.appointment.id)
  }

  closeModalSave() {
    this.modalSave.nativeElement.style.display = 'none';
  }

  showModalSave() {
    this.modalSave.nativeElement.style.display = 'block';
  }

  savePatient() {
    this.patientService.saveUnregistered(this.appointment.id, this.password);
  }

}
