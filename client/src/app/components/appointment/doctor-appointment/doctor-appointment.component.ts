import { Component, OnInit } from '@angular/core';
import {Appointment} from "../../../models/appointment.model";
import {AppointmentService} from "../../../services/appointment.service";
import {AuthService} from "../../../services/auth.service";
import {User} from "../../../models/user.model";

@Component({
  selector: 'app-doctor-appointment',
  templateUrl: './doctor-appointment.component.html',
  styleUrls: ['./doctor-appointment.component.css']
})
export class DoctorAppointmentComponent implements OnInit {
  appointments: Appointment[] = [];
  loggedUser: User;

  constructor(private appService: AppointmentService,
              private authService: AuthService) { }

  ngOnInit() {
    this.appService.getAll((apps: Appointment[]) => {
      this.appointments = apps
    });

    this.loggedUser = this.authService.getAuthenticatedUser();
  }

}
