import {Component, OnInit} from '@angular/core';
import {Assistance} from "../../../models/assistance.model";
import {AssistanceService} from "../../../services/assistance.service";
import {PatientService} from "../../../services/patient.service";
import {LogService} from "../../../services/log.service";
import {AppointmentService} from "../../../services/appointment.service";
import {Appointment} from "../../../models/appointment.model";
import {Router} from "@angular/router";
import {SerializableDate} from "../../../utils/serializable-date";

@Component({
  selector: 'app-appointment-form',
  templateUrl: './appointment-form.component.html',
  styleUrls: ['./appointment-form.component.css']
})
export class AppointmentFormComponent implements OnInit {
  assistances: Assistance[] = [];
  // form data
  selectedDate: Date;
  selectedAssistances: number[] = [];
  cnp: string;
  firstName: string;
  lastName: string;
  unregisteredPatient: boolean = false;

  constructor(private assService: AssistanceService,
              private patientService: PatientService,
              private appService: AppointmentService,
              private router: Router,
              private log: LogService) {
  }

  ngOnInit() {
    this.assService.getAll((result: Assistance[]) => this.assistances = result);
  }

  toggleNewPatient() {
    this.unregisteredPatient = !this.unregisteredPatient;
  }

  formHasValidData() {
    if(!this.selectedDate) {
      this.log.error('Please select a date first');
      return false;
    }

    if(!this.cnp) {
      this.log.error('Please write a CNP first');
    }

    if(this.unregisteredPatient && (!this.firstName || !this.lastName)) {
      this.log.error('Please write both first and last names');
      return false;
    }

    if(this.selectedAssistances.length < 1) {
      this.log.error('You must select at least one assistance');
      return false;
    }

    return true;
  }

  createAppointment() {
    if(!this.formHasValidData()) {
      return;
    }

    const appData = {
      patientId: 0,
      assistanceIds: this.selectedAssistances,
      time: SerializableDate.serialize(this.selectedDate.getTime() + 1000 * 60 * 60 * 2),
      registeredPatient: !this.unregisteredPatient,
      unregisteredPatient: {
        firstName: this.firstName,
        lastName: this.lastName,
        cnp: this.cnp
      }
    };

    if(this.unregisteredPatient) {
      return this.appService.save(appData, (app: Appointment) => this.router.navigate(['/appointment/' + app.id]));
    }

    this.patientService.getByCnp(this.cnp, (patient) => {
      appData.patientId = patient.id;
      this.appService.save(appData, (app: Appointment) => this.router.navigate(['/appointment/' + app.id]));

    }, () => {
      this.log.error('Could not find a patient with such cnp')
    })


  }

}
