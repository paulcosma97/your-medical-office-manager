import {Component, Input, OnInit} from '@angular/core';
import {Appointment} from "../../../models/appointment.model";
import {AppointmentService} from "../../../services/appointment.service";

@Component({
  selector: 'app-appointment-table',
  templateUrl: './appointment-table.component.html',
  styleUrls: ['./appointment-table.component.css']
})
export class AppointmentTableComponent implements OnInit {
  @Input() appointments: Appointment[] = [];
  @Input() canCreateNew: boolean = true;
  searchFilter: string = "";

  constructor(private appService: AppointmentService) { }

  ngOnInit() {
    this.appointments = this.appointments.sort(((a: Appointment, b: Appointment) => a.time < b.time ? -1 : 1));
  }

  filter(apps: Appointment[]): Appointment[] {
    return apps.filter(app => {
      if(app.patient) {
        return app.patient.lastName.toLocaleLowerCase().includes(this.searchFilter.toLocaleLowerCase()) ||
          app.patient.firstName.toLocaleLowerCase().includes(this.searchFilter.toLocaleLowerCase()) ||
          app.patient.cnp.toLocaleLowerCase().includes(this.searchFilter.toLocaleLowerCase());
      }

      if(app.unregisteredPatient) {
        return app.unregisteredPatient.lastName.toLocaleLowerCase().includes(this.searchFilter.toLocaleLowerCase()) ||
          app.unregisteredPatient.firstName.toLocaleLowerCase().includes(this.searchFilter.toLocaleLowerCase()) ||
          app.unregisteredPatient.cnp.toLocaleLowerCase().includes(this.searchFilter.toLocaleLowerCase());
      }

    })
  }

}
