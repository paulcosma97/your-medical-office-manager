import {Route} from '@angular/router';
import {LoginComponent} from '../components/login/login.component';
import {AuthGuard} from '../guards/auth.guard';
import {HomeComponent} from '../components/home/home.component';
import {AnonymousGuard} from '../guards/anonymous.guard';
import {DoctorGuard} from "../guards/doctor.guard";
import {DoctorAppointmentComponent} from "../components/appointment/doctor-appointment/doctor-appointment.component";
import {AppointmentDetailsComponent} from "../components/appointment/appointment-details/appointment-details.component";
import {AppointmentFormComponent} from "../components/appointment/appointment-form/appointment-form.component";
import {PatientComponent} from "../components/patient/patient/patient.component";
import {PatientDetailsComponent} from "../components/patient/patient-details/patient-details.component";
import {AssistanceComponent} from "../components/assistance/assistance/assistance.component";
import {AssistanceDetailsComponent} from "../components/assistance/assistance-details/assistance-details.component";
import {AdminGuard} from "../guards/admin.guard";
import {DoctorListComponent} from "../components/admin/doctor-list/doctor-list.component";

export const Routes: Route[]  = [
    {path: '', redirectTo: '/home', pathMatch: 'full'},
    {path: 'login', component: LoginComponent, canActivate: [AnonymousGuard]},
    {path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
    {path: 'appointment', component: DoctorAppointmentComponent, canActivate: [AuthGuard]},
    {path: 'appointment/new', component: AppointmentFormComponent, canActivate: [AuthGuard, DoctorGuard]},
    {path: 'appointment/:id', component: AppointmentDetailsComponent, canActivate: [AuthGuard]},
    {path: 'assistance', component: AssistanceComponent, canActivate: [AuthGuard]},
    {path: 'assistance/:id', component: AssistanceDetailsComponent, canActivate: [AuthGuard, DoctorGuard]},
    {path: 'patient', component: PatientComponent, canActivate: [AuthGuard]},
    {path: 'user/:id', component: PatientDetailsComponent, canActivate: [AuthGuard]},
    {path: 'doctor', component: DoctorListComponent, canActivate: [AuthGuard, AdminGuard]},
    {path: 'doctor/:id', component: HomeComponent, canActivate: [AuthGuard, AdminGuard]},
  ];
