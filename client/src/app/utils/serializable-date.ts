export class SerializableDate {
  static serialize(args: any): string {
    const date = new Date(args);
    return date.toJSON().replace('T', ' ').substr(0, date.toJSON().length - 5);
  }
}
