import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './components/root/app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import { RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import {Routes} from './utils/routes';
import { HomeComponent } from './components/home/home.component';
import { NotificationComponent } from './components/notification/notification.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DoctorAppointmentComponent } from './components/appointment/doctor-appointment/doctor-appointment.component';
import { AppointmentTableComponent } from './components/appointment/appointment-table/appointment-table.component';
import {AuthHttpInterceptor} from "./utils/http-interceptor.service";
import { AppointmentDetailsComponent } from './components/appointment/appointment-details/appointment-details.component';
import { AssistanceTableComponent } from './components/assistance/assistance-table/assistance-table.component';
import { AppointmentFormComponent } from './components/appointment/appointment-form/appointment-form.component';
import {DlDateTimePickerDateModule} from "angular-bootstrap-datetimepicker";
import { PatientComponent } from './components/patient/patient/patient.component';
import { UserTableComponent } from './components/patient/user-table/user-table.component';
import { PatientDetailsComponent } from './components/patient/patient-details/patient-details.component';
import { AssistanceComponent } from './components/assistance/assistance/assistance.component';
import { AssistanceDetailsComponent } from './components/assistance/assistance-details/assistance-details.component';
import { DoctorListComponent } from './components/admin/doctor-list/doctor-list.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    NotificationComponent,
    NavbarComponent,
    DoctorAppointmentComponent,
    AppointmentTableComponent,
    AppointmentDetailsComponent,
    AssistanceTableComponent,
    AppointmentFormComponent,
    PatientComponent,
    UserTableComponent,
    PatientDetailsComponent,
    AssistanceComponent,
    AssistanceDetailsComponent,
    DoctorListComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    DlDateTimePickerDateModule,
    RouterModule.forRoot(Routes, {onSameUrlNavigation: "reload"})
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHttpInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
