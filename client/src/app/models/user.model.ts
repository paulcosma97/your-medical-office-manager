export class User {
  id: number;
  cnp: string;
  firstName: string;
  password: string;
  lastName: string;
  birthday: Date;
  roles: string[];
}

export class Report {
  totalAmount: number;
  lastYear: number;
  lastMonth: number;
  lastDay: number;
}
