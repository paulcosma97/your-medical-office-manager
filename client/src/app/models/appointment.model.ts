import {User} from "./user.model";
import {Assistance} from "./assistance.model";

export class Appointment {
  id: number;
  doctor: User;
  patient: User;
  unregisteredPatient: UnregisteredPatient;
  time: Date;
  assistances: Assistance[];
  registeredPatient: boolean;
}

export class UnregisteredPatient {
  firstName: string;
  lastName: string;
  cnp: string;
}
